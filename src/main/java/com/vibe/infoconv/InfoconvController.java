package com.vibe.infoconv;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/infoconv")
public class InfoconvController {
	
	@Autowired
	private Environment environment;

	@RequestMapping(value = "/home",produces = "text/html")
    public String home() {
        return "<h1>INFOCONV</h1><p><h2>INFOCONV API</h2></p><p>Clique na <a href=\"/infoconv/cpfs\">lista</a> para " +
            "obter um CPF</p>";
    }

    @RequestMapping(value = "/cpfs", produces = "application/json")
    public List<String> cpfs() {
        return Arrays.asList("57089933245", "68111889364", "01775658457", "89076387214");
    }
    
    @RequestMapping(value = "/configmap", produces = "application/json")
    public List<String> configmap() {
        return Arrays.asList(environment.getProperty("app.datasource.infoconv.url"), environment.getProperty("app.datasource.infoconv.jndi-name"));
    }
    
    
    @RequestMapping(value = "/secret", produces = "application/json")
    public List<String> secret() {
        return Arrays.asList(environment.getProperty("app.datasource.infoconv.username"), environment.getProperty("app.datasource.infoconv.password"));
    }
    
    @RequestMapping(value = "/isAlive",produces = "text/html")
    public String isAlive() {
        return "ok";
    }
    
}
