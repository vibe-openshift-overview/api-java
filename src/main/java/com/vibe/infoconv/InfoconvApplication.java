package com.vibe.infoconv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InfoconvApplication {

	public static void main(String[] args) {
		SpringApplication.run(InfoconvApplication.class, args);
	}
}
